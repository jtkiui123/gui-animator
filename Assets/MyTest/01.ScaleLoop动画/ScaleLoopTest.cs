﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleLoopTest : MonoBehaviour {

	public GEAnim m_buttonAnim;
	// Use this for initialization
	void Start ()
	{
#if UNITY_EDITOR
		if(GEAnimSystem.Instance.m_TestMoveIn==true)
			GEAnimSystem.Instance.m_TestMoveIn = false;
		if(GEAnimSystem.Instance.m_TestMoveOut==true)
			GEAnimSystem.Instance.m_TestMoveOut = false;
#endif
		m_buttonAnim.MoveIn();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
