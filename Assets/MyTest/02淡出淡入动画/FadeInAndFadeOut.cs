﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInAndFadeOut : MonoBehaviour {

	public GEAnim m_imageAnim;
	public GEAnim m_fadeInAndOutAnim;
	// Use this for initialization
	void Start () {
#if UNITY_EDITOR
		if (GEAnimSystem.Instance.m_TestMoveIn == true)
			GEAnimSystem.Instance.m_TestMoveIn = false;
		if (GEAnimSystem.Instance.m_TestMoveOut == true)
			GEAnimSystem.Instance.m_TestMoveOut = false;
#endif
		m_imageAnim.MoveIn();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			m_fadeInAndOutAnim.MoveIn();
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			m_fadeInAndOutAnim.MoveOut();
		}
	}
}
